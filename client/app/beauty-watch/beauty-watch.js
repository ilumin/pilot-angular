'use strict';

angular.module('pilotAngularApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('beautyWatch', {
                url: '/',
                templateUrl: 'app/beauty-watch/beauty-watch.html',
                controller: 'beautyWatchCtrl'
            });
    });